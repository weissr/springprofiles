## Examples of overriding spring property value

Following commands should be executed next to jar file, that will appear in `target` folder after project build.

Build project `.\mvnw.cmd clean install`

Navigate to targets `cd target`

### 1.Define value as environment variable

- for Powershell - `$env:PROFILEDEMO_SETTINGS="Overriden by ENV variable"`
- for CMD `set PROFILEDEMO_SETTINGS="Overriden by ENV variable"`
- for UNIX `PROFILEDEMO_SETTINGS="Overriden by ENV variable"`

`java -jar .\profiledemo-0.0.1-SNAPSHOT.jar`

### 2.Pass value as argument to application

`java -jar .\profiledemo-0.0.1-SNAPSHOT.jar --profiledemo.settings="overriden by argumet"`

### 3.Pass value as java system property

`java "-Dprofiledemo.settings='overriden by java system property'" -jar .\profiledemo-0.0.1-SNAPSHOT.jar` it is like we
do in beanstalk adding values to `JAVA_OPTS`

For
reference: https://docs.spring.io/spring-boot/docs/2.6.1/reference/html/features.html#features.external-config.application-json
