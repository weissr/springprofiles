package com.weissr.profiledemo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Slf4j
@RequiredArgsConstructor
@Component
public class ProfileInfoPrinter {
    private final Environment environment;

    @PostConstruct
    public void logInf() {
        printVarValue("profiledemo.settings");
        printVarValue("profiledemo.url");
        printVarValue("profiledemo.admin.email");
        printVarValue("profiledemo.base-file-path");
        printVarValue("spring.datasource.url");
    }

    private void printVarValue(String key) {
        log.info(key + ": " + environment.getProperty(key));
    }
}
